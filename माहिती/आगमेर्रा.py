import asks
import trio

from .माहिती import माहिती


class आगमेर्रा(माहिती):
    वर्ष = (1980, 2010)

    async def प्राप्त_करणे(स्वतः, वर्ष, परिवर्तनशील):
        url = f'https://data.giss.nasa.gov/impacts/agmipcf/agmerra/AgMERRA_{वर्ष}_{परिवर्तनशील}.nc4'

        r = await asks.get(url, stream=True)
        async with await trio.open_file(arch_local, 'ab') as d:
            async with r.body:
                async for pedazobits in r.body:
                    d.write(pedazobits)
