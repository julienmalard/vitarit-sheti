from .माहिती import माहिती


class एसआरतीएम(माहिती):
    def यूआरएल(स्वतः, वर्ष, परिवर्तनशील, अकांश, रेखांश):
        y = int((60 - अकांश) // 5 + 1)
        x = int((रेखांश + 180) // 5 + 1)
        return f'http://srtm.csi.cgiar.org/wp-content/uploads/files/srtm_5x5/TIFF/srtm_{x:02}_{y:02}.zip'
