from .माहिती import माहिती


class फाओ_मिठी(माहिती):
    'http://www.fao.org/3/aq361e/aq361e.pdf'

    उंची = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloElev_30as.rar'

    pend0_05 = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloSlopesCl1_30as.rar'
    pend05_2 = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloSlopesCl2_30as.rar'
    pend2_5 = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloSlopesCl3_30as.rar'
    pend5_10 = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloSlopesCl4_30as.rar'
    pend10_15 = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloSlopesCl5_30as.rar'
    pend15_30 = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloSlopesCl6_30as.rar'
    pend30_45 = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloSlopesCl7_30as.rar'
    pend45_plus = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloSlopesCl8_30as.rar'
    aspecto_N = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloAspectClN_30as.rar'
    aspecto_E = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloAspectClE_30as.rar'
    aspecto_S = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloAspectClS_30as.rar'
    aspecto_O = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloAspectClO_30as.rar'
    aspecto_U = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GloAspectClU_30as.rar'

    uso_lluvia = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Land_Cover_data/CULT_2000.asc'
    uso_irrig = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Land_Cover_data/CULTIR_2000.asc'
    uso_total = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Land_Cover_data/CULTRF_2000.asc'
    uso_bosq = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Land_Cover_data/FOR_2000.asc'
    uso_pasto = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/GRS_2000.asc'
    uso_urb = 'http://www.fao.org/fileadmin/user_upload/soils/HWSD%20Viewer/URB_2000.asc'
    uso_seco = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Land_Cover_data/NVG_2000.asc'
    uso_agua = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Land_Cover_data/WAT_2000.asc'

    calidad_nutr = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Soil_Quality_data/sq1.asc'
    calidad_ret_nutr = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Soil_Quality_data/sq2.asc'
    calidad_raíz = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Soil_Quality_data/sq3.asc'
    calidad_O2 = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Soil_Quality_data/sq4.asc'
    calidad_sal = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Soil_Quality_data/sq5.asc'
    calidad_tox = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Soil_Quality_data/sq6.asc'
    calidad_trab = 'http://www.fao.org/fileadmin/user_upload/soils/docs/HWSD/Soil_Quality_data/sq7.asc'
